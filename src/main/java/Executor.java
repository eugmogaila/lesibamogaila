import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class Executor {

 public static WebDriver driver;
 public static Map<String, String> map = new HashMap<String, String>();
    public static ExtentReports extent;
    public static ExtentTest logger;




    public static String webUrl;



    public Executor()
    {

    }


   public static void Start_Execution()
   {
       Executor exec = new Executor();
       exec.loadObjectFile();
       exec.GetPomAttributes();
       String driverPath="./src/Framework/WebDriver/";
       System.setProperty("webdriver.chrome.driver",driverPath+"chromedriver.exe");
       driver = new ChromeDriver();
   }


   public void SetTestName(String name)
   {
       logger = extent.startTest(name);
   }


   public void TextIsValid(String obj,String  msg)
   {
       if(driver.findElement(By.xpath(map.get(obj))).isDisplayed() &&getObjectAttribute(obj,"text").equals(msg))
       {
           logger.log(LogStatus.PASS, "Message displayed as expected : "+obj);
       }
       else
       {
           logger.log(LogStatus.FAIL,"Message displayed does not match");
       }
   }

   public void GoToURL()
   {
       String url ="https://www.ilabquality.com/";
       driver.manage().window().maximize();
       driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
       logger.log(LogStatus.PASS, "Navigate to project Url : "+url);
       driver.navigate().to(url);
   }


 public static void Click(String obj)
 {
     if(driver.findElement(By.xpath(map.get(obj))).isDisplayed())
     {
         logger.log(LogStatus.PASS, "Click on object : "+obj);
         driver.findElement(By.xpath(map.get(obj))).click();
     }
     else
     {
         logger.log(LogStatus.PASS,"Could not click element "+obj);
     }
 }


 public void Type(String obj,String val)
 {
     if(driver.findElement(By.xpath(map.get(obj))).isDisplayed())
     {
         logger.log(LogStatus.PASS, "Type vale: "+val+"on object:"+obj);
         driver.findElement(By.xpath(map.get(obj))).sendKeys(val);
     }
     else
     {
         logger.log(LogStatus.PASS,"Could type on element "+obj);
     }

 }

 public String getObjectAttribute(String obj, String attr){

     return driver.findElement(By.xpath(map.get(obj))).getAttribute(attr);
 }

 public static void KillDriver()
 {
     driver.quit();
 }


 private   Map<String, String> loadObjectFile()
 {
     String[] pairs = mappedObjects().split(";");

     for (String string : pairs) {
         String[] keyValue = string.split("~");
         map.put(keyValue[0], keyValue[1]);
     }
     return map;
 }

 private String mappedObjects()
 {
     try
     {


         File file = new File("./src/Framework/mapping/objectsFile.txt");

         BufferedReader br = new BufferedReader(new FileReader(file));

         StringBuilder sb = new StringBuilder();

          br.readLine();
         String st;
         while ((st=br.readLine()) != null)
             sb.append(st);

         return sb.toString();

     }
     catch (Exception e)
     {
      return "File not found";
     }

 }




 public void GetPomAttributes()
 {
     try
     {
         Properties prop = new Properties();
         String fileName = "./src/main/resources/TestConfig.tld";
         InputStream is = new FileInputStream(fileName);

         prop.load(is);

         System.out.println(prop.getProperty("uri"));
     }
     catch (Exception e)
     {

     }


 }




}

package TestModel;

import java.io.File;
import java.io.FileInputStream;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;


public class Excel {

    public static Object[][] excelDataProviderMethod(String folder) {

        System.out.println("Excel");
        Object[][] retObjArr = { { folder } };
        try {
            File path = new File("");
            FileInputStream file = new FileInputStream(
                    new File( "./src/Framework/Data/TestData1.xlsx"));
            XSSFWorkbook workbook = new XSSFWorkbook(file);
            XSSFSheet sheet = workbook.getSheet("Details");

            int rowCount = sheet.getPhysicalNumberOfRows() - 1;
            int colCount = sheet.getRow(0).getPhysicalNumberOfCells();


            retObjArr = new Object[rowCount][colCount];
            for (int rownum = 0; rownum < rowCount; rownum++) {
                Row row = sheet.getRow(rownum + 1);

                for (int col = 0; col < colCount; col++) {
                    retObjArr[rownum][col] = row.getCell(col).getStringCellValue();
                }
            }
            file.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }

        System.out.println("Done");

        return retObjArr;
    }


    @DataProvider(name = "Applicant Details")
    public static Object[][] ApplicantDetails() {
        return excelDataProviderMethod("./src/Framework/Data/");
    }




}

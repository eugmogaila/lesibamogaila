import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.commons.io.FileUtils;
import org.testng.annotations.*;

public class BaseTest extends Executor {

    @BeforeClass
    public static void start() {


        String hostname = "Unknown";
        try {
            InetAddress addr;
            addr = InetAddress.getLocalHost();
            hostname = addr.getHostName();
        } catch (UnknownHostException ex) {
            System.out.println("Hostname can not be resolved....");
        }

        extent = new ExtentReports("./test-output/Assessment.html", true);
        extent.addSystemInfo("Environment", "Quality Assurance").addSystemInfo("Host Name", hostname)
                .addSystemInfo("Environment", "QA").addSystemInfo("User Name", System.getProperty("user.name"))
                .addSystemInfo("Project", "ILAB Website");
        extent.loadConfig(new File("./extent-config.xml"));





        Start_Execution();
    }





    @AfterMethod
    public void getResult(ITestResult result) throws IOException {
        String ImagePath = logger.addScreenCapture(getScreenShot(driver, result.getName()));


        if (result.getStatus() == ITestResult.FAILURE) {
            logger.log(LogStatus.FAIL, "Test case failed is " + result.getName(), ImagePath);
            logger.log(LogStatus.FAIL, result.getName() + " failed due to:\n", result.getThrowable());
        }

        if (result.getStatus() == ITestResult.SKIP) {
            logger.log(LogStatus.SKIP, "Test case skipped is " + result.getName(), result.getThrowable());
        }

        if (result.getStatus() == ITestResult.SUCCESS)
        {
            logger.log(LogStatus.PASS, "Test case passed" + result.getName(), ImagePath);
        }

    }



    @AfterSuite
    public static void closeReport() {
        extent.flush();
        processReport();
    }










    @AfterClass
    public static void end() {
        KillDriver();
    }


    public static void processReport()
    {
        System.out.println("Creating report directories");
        File main = new File("./test-output/img");
        boolean successful = main.mkdirs();
        if (successful)
        {
            System.out.println("Report directiry created");
        }
        else{System.out.println("failed trying to create the directory");}
    }



    // The following method take screenshot and return the path to that screenshot
    private static String getScreenShot(WebDriver driver, String fileName) throws IOException {

        DateFormat df = new SimpleDateFormat("yyyyMMdd'T'HHmmss");
        Date dateobj = new Date();
        String TransactionDate = ((df.format(dateobj)));

        fileName = fileName + TransactionDate + ".png";
        String directory = System.getProperty("user.dir") +"/test-output/img/" + fileName;

        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile, new File(directory));

        String destination = directory;

        return destination;
    }




}

import TestModel.Excel;
import org.testng.annotations.Test;


public class SubmitApplication  extends BaseTest{


    @Test(priority = 1, dataProvider = "Applicant Details", dataProviderClass = Excel.class)
    public void SubmitApplicationForm(String name,String email,String phone,String message)
    {
        SetTestName("Submit online application");
        GoToURL();
        Click("ntbCareers");
        Click("lblScouthAfric");
        Click("lblJobOpening");
        Click("btnApplyNow");
        Type("txtName",name);
        Type("txtEmail",email);
        Type("txtPhone",phone);
        Type("txtMessage",message);
        Click("btnSendAppllication");
        TextIsValid("txtErrorMessage","There are errors in your form.");
    }

}
